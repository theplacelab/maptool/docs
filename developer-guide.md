# Running Maptool

## Overview

The maptool is comprised of a web front-end written in React which is supported by a number of microservices. All of these services expect to communicate with one or more cloud service providers. For instance in order to log into the website you need to run an instance of the **authorization service** which should be configured to send email via [Sendgrid](https://sendgrid.com/) or [Mailgun](https://www.mailgun.com/). In order to store media at points of interest on the map, you need to run an instance of the **asset service** which should be configured to work with an S3 bucket such as [AWS](https://aws.amazon.com/s3/) or [Wasabi](https://wasabi.com/).

## Do I need to sign up for anything?

- **Hosting Provider**  
  Any hosting provider which can handle both node and static websites should do. Docker images are provided. For most of our projects, we run everything together on a kubernetes cluster. It should also be possible to run things "bare metal" or distributed across several servers.

- **Postgres Provider**  
  Optional but recommended. The three core services (_authorization_, _asset_ and _map_) will configure themselves to use SQLite "locally" as a datastore. This is useful to get things started or for small projects, but if you are hosting something online chances are you'll like to upgrade to a more robust database service.

- **Cloudflare DNS**  
  Optional but recommended. Cloudflare DNS is optional but it can be extremely helpful for maps which support media - The edge caching greatly speeds up loading time and lowers bandwidth costs significantly.

- **S3: Wasabi or AWS**  
  S3 is the file storage engine of choice for the maptool asset service. You may use any S3 compatible service: we tend to use Wasabi because the costs are easier to estimate. AWS is also an option, as is any third party S3 service.

- **Sendgrid OR Mailgun**  
  Many user management tasks (account validation, password resets, etc) are accomplished via email for security. An email provider is required. We've found that either Sendgrid or Mailgun perform well at the free tier and provide sample configurations for both.

- **Mailtrap**  
  Optional but recommended. Configuration and setup of an email service can be tricky. Mailtrap will let you test your maptool setup easily so you can be confident Maptool is working before troublshooting mail routing.

  > We cannot predict how you will use this tool, but in most cases we have been able to take advantage of the free or lowest-cost tier of all of the above services and been quite happy with the results. One of the main reasons for architecting Maptool in this way (as a series of modular services) is that you can mix and match: If you are so inclined you may swap any of the services described for your own or future services as long as they observe the same API. If you do add support for something interesting, please file a pull-request!

## Is there an "easy setup?"

The Maptool is a set of software tools and not a commercial hosting service. There is no "maptool in a box." However, we run everything on Kubernetes and have provided docker images and containers for all services and the front end. The fastest way to a working setup: 1. Create accounts and gather the various API keys and account details from the services mentioned above. 2. Deploy everything onto a Kubernetes cluster using [this configuration](/k8-guide).

## Front End

To deploy the front end:

## Back End

### Three Core Services

### Authorization

| METHOD | ENDPOINT         | BEARER              |
|:-------|------------------|---------------------|
| POST   | login/           |                     |
| POST   | reauthorize/     |                     |
| PUT    | user/            |                     |
| POST   | user/status      |                     |
| POST   | user/reset       |                     |
| GET    | user/:id         | 🔒 super or self    |
| DELETE | user/:id         | 🔒 super or self    |
| PATCH  | user/:id         | 🔒 super or self    |
| POST   | user/validate    | 🔒 validation token |
| POST   | user/changepass  | 🔒 self             | 
| GET    | users/           | 🔒 super            |

```sh
# Used by server-postgrest and server-node
DEBUG=
JWT_SECRET=
CLIENT_BASE_URL=http://localhost:3000
USER_API_URL=https://example.com/auth

PG_SSL_STRICT=
PG_CONNECTION_STRING=

# Used by server-node for sending email
MAIL_FROM_NAME=
MAIL_FROM_EMAIL=
MAIL_SENDGRID_API_KEY=
MAIL_MAILTRAP_USER=
MAIL_MAILTRAP_PASS=
MAIL_MAILGUN_API_KEY=
MAIL_MAILGUN_DOMAIN=

MAIL_TEMPLATE_COPYRIGHT=
MAIL_TEMPLATE_UNSUBSCRIBE_LINK=''
MAIL_TEMPLATE_MAILING_ADDRESS='This is an automated message from ~FROM_NAME~ (~FROM_EMAIL~), please do not reply. If you did not request this email, you can ignore it.'
MAIL_TEMPLATE_SERVICE_NAME=
MAIL_TEMPLATE_SERVICE_LINK=
MAIL_VALIDATION_ENDPOINT=
```

### Assets

| METHOD | ENDPOINT                  | BEARER              |
|:-------|---------------------------|---------------------|
| GET    | assets/                   | 🔒 auth token       |
| POST   | assets/                   | 🔒 auth token       |
| PUT    | asset/                    | 🔒 auth token       |
| GET    | asset/:id                 | 🔒 auth token       |
| DELETE | asset/:id                 | 🔒 auth token       |
| PATCH  | asset/:id                 | 🔒 auth token       |
| PUT    | asset/replace/:asset_path | 🔒 auth token       |
| PUT    | assets/import             | 🔒 auth token       |
| GET    | assets/export             | 🔒 auth token       |

```sh
DEBUG=true
PORT=8888

AWS_ENDPOINT=s3.wasabisys.com
AWS_REGION=us-east-1
AWS_APP_ID=staging
AWS_BUCKET=

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
JWT_SECRET=

CLOUDFLARE_ZONE=
CLOUDFLARE_TOKEN=
CLOUDFLARE_EMAIL=
PG_SSL_STRICT=false
PG_CONNECTION_STRING=
```

### Map
| METHOD | ENDPOINT                  | BEARER              |
|:-------|---------------------------|---------------------|
| GET    | maps/                     | 🔒 auth token       |

| PUT    | map/                      | 🔒 auth token       |
| GET    | map/:id                   | 🔒 auth token       |
| DELETE | map/:id                   | 🔒 auth token       |
| PATCH  | map/:id                   | 🔒 auth token       |
 
| GET    | map/settings              | 🔒 auth token       |
| PATCH  | map/settings              | 🔒 auth token       |

| GET    | map/:id/settings          | 🔒 auth token       |
| PATCH  | map/:id/settings          | 🔒 auth token       |

| GET    | map/:id/poi/config        | 🔒 auth token       |
| PATCH  | map/:id/poi/config        | 🔒 auth token       |

| PUT    | map/:id/poi/              | 🔒 auth token       |
| GET    | map/:id/poi/:id           | 🔒 auth token       |
| DELETE | map/:id/poi/:id           | 🔒 auth token       |
| PATCH  | map/:id/poi/:id           | 🔒 auth token       |

```sh
DEBUG=true
PORT=8888
JWT_SECRET=
```
