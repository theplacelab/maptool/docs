# Development

This documentation exists to give a high-level overview of the architecture of the project. Please see the README.md file in each indivdual repository for up-to-date documentation. Any discrepencies should assume the README in each repository is up to date.

## Client (Frontend)

[Maptool]()

## Service (Backend)

### [Assets](https://gitlab.com/theplacelab/service/assets)
### [Auth](https://gitlab.com/theplacelab/service/auth)  
### [Map](https://gitlab.com/theplacelab/service/map)  
