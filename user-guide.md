# User Guide

## Preview

## Maps

## Map Style JSON

> Please read this section carefully. Selecting the right map style URL determines how your map appears but also has legal, licensing and financial implications

At their most basic level, maps on the internet consist of a set of tiles: pictures or drawings of a spot on the planet earth or, in the case of fantasy maps or document

The key to setting up any map with the Maptool is a valid configuration defined using the [MapLibre GL Style Specification](https://maplibre.org/maplibre-gl-js-docs/style-spec/). The most important thing that this file does is define the "tileset" that the map will use: what images compose the map? What color are they?  their color, format and various overlays (roads, buildings). 

### Tileset: Vector or Raster?
If you are unsure and you have a choice, go with vector. Briefly: Raster images consist of actual pictures (like a photograph) whereas vector image data con. Vector is almost always faster, smoother to animate and display. In some cases 

### Where do I get one of these?
You have several options: 
1. You can paste in a URL from [MapTiler](https://cloud.maptiler.com/). Not free but the easiest.

2. You can paste in a URL from a different source. There aren't any but we'll offer a few 

```
{
      "layers":[
         {
            "id":"watercolor",
            "type":"raster",
            "source":"watercolor"
         }
      ],
      "sources":{
         "watercolor":{
            "type":"raster",
            "tiles":[
               "https://stamen-tiles.a.ssl.fastly.net/terrain/{z}/{x}/{y}.jpg"
               //"https://assets.theplacelab.com/tiles/wipzine/page-02.png/{z}/{x}/{y}.png"
            ],
            "attribution":"Your Mother"
         }
      },
      "version":8
   }
```

3. You can create your own! [The standard is here]((https://maplibre.org/maplibre-gl-js-docs/style-spec/)) or you can use one of the above examples and modify it. 

## Assets

## Users

## Settings