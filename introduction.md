# What is The Maptool?



"The Maptool" refers to a collection of tools, techniques and software which allow for the creation of projects involving placing media at coordinate locations on a map.

The primary entry point for interaction is a React-based web application which provides an interface to a number of microservices written in Node.

One can take advantage of the web application to create and explore interactive maps, or opt to use the backend alone to facilitate the creation of native mobile applications. The web tool may also be wrapped or extended in order to customize the experience. 



## Citations
> Sempere, Anindita Basu, and Andrew Sempere.  
2023. “Deep Maps, Authorship, and Narrativizing
Physical Spaces,” in “DH Unbound 2022,
Selected Papers,”  ed. Barbara Bordalejo, Roopika
Risam, and Emmanuel Château-Dutier, special
issue. Digital Studies/Le champ numérique 13(3):
1–15. DOI: https://doi.org/10.16995/dscn.9662.


## Example Projects